  ls_node-name              = gc_nd_ppm_object. " Name des knotens
  ls_node-name_parent       = gc_nd_ppe_object. " Elternknoten 
  ls_node-transient         = abap_false.		
  ls_node-update_relevant   = abap_true.		
  ls_node-id_struc          = '/XPLM/PSM_PPM_TS_PROJECT_DATAI'. " Struktur zur Identifizierung
  ls_node-data_struc        = '/XPLM/PSM_PPM_TS_PROJECT_DATA'.  " Datenstruktur
  ls_node-update_sideeffect = /plmb/if_mdp_c=>gc_c_sideeffect-myself_child.
  ls_node-insert_sideeffect = /plmb/if_mdp_c=>gc_c_sideeffect-MYSELF.
  ls_node-delete_sideeffect = /plmb/if_mdp_c=>gc_c_sideeffect-myself_child.

  INSERT ls_node INTO TABLE CT_METADATA_NODE. "An den MP der Standardanwendung übergeben.